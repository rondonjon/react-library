# RonDonJon's React Library

A small collection of themable, scalable, and otherwise customizable React components, written in Typescript and bundled as ES module.

<img src="docs/screenshot.png" alt="Screenshot" width="400" height="385" />

Demo: https://rondonjon.gitlab.io/react-library/

## License

Copyright © 2022 "Ron Don Jon" Daniel Kastenholz.

Released under the terms of the [AGPL-3.0](https://www.gnu.org/licenses/agpl-3.0.en.html).

## History

| Version | Changes                                                                                                    |
| ------- | ---------------------------------------------------------------------------------------------------------- |
| 0.1.0   | Add `Button` component                                                                                     |
| 0.2.0   | Add `Paragraph` and `Monospace` components                                                                 |
| 0.3.0   | Add `Area` and `Headline` components; introduce `dark` and `light` themes                                  |
| 0.3.1   | Fix package metadata and display it in the Demo                                                            |
| 1.0.0   | Prepare package for publishing; release on npm                                                             |
| 1.1.0   | Add `Link` component                                                                                       |
| 1.2.0   | Add `Splitter` component and `useClientRect()` hook                                                        |
| 1.3.0   | Rework demo page with `Splitter` layout and individual demo pages                                          |
| 1.4.0   | Improve legibility and layout in demo app                                                                  |
| 1.5.0   | Make demo app responsive; forbid unused locals; add more hooks; add missing components to library builds   |
| 1.6.0   | Add math functions and ava tests                                                                           |
| 2.0.0   | Refactor theme styles; new px/rem use; configure absolute vs. text-relative units for non-text sizes       |
| 2.1.0   | Skip 300ms delay after `Link`/`Button` click on mobile                                                     |
| 2.1.1   | Fix `Splitter` on mobile devices                                                                           |
| 2.2.0   | Use compact menu layout in Demo app on mobile devices                                                      |
| 2.3.0   | Add `Color` tests                                                                                          |
| 2.4.0   | Unify `text-shadow`; include text scaling in `Button` component `border-radius`; bigger `Splitter` bar     |
| 2.4.1   | Refactor `Splitter` to absolute layout to fix occasional disappearance of the component on the `b` size    |
| 2.5.0   | Introduce `fill` style attribute to configure backgrounds and borders independently in `Button` and `Area` |
| 2.5.1   | Add missing `fill`/`slope`/`light` styles to `Splitter`, refine style rules in others                      |
| 2.6.0   | Add `RadioButton` and `Label` components                                                                   |
| 2.6.1   | Add `eslint` to project, fix missing hook dependencies, add missing `RadioButton` `disabled` state         |
| 2.7.0   | Add `Checkbox` component                                                                                   |
| 2.7.1   | Fix broken `value` handling in `RadioButton`                                                               |
| 2.8.0   | Update README                                                                                              |
