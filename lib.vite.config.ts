import { defineConfig } from "vite"
import { writeFileSync, unlinkSync } from "fs"
import { resolve } from "path"
import glob from "glob"
import react from "@vitejs/plugin-react"
import { generateDtsBundle } from "rollup-plugin-dts-bundle-generator"
import { Plugin } from "rollup"

const pkg = require("./package.json")
const external = Object.keys(pkg.dependencies)

/**
 * This plugin generates a src/lib.ts entry file for the library build
 * and popuplates it with all relevant export statements. The file is
 * deleted after the build so that it won't confuse VS Code / Intellisense;
 * their default behavior for automcomplete is to import from the topmost
 * (i.e. the lib.ts) file, which would result in circular dependencies;
 * I am working around this problem by not having such a file at all.
 */
const temporaryLibEntry = (): Plugin => ({
  name: "temporary-lib-entry",
  buildStart: () => {
    const filenames = [
      ...glob.sync("src/components/*/index.tsx"),
      ...glob.sync("src/components/*/types/*.ts"),
      ...glob.sync("src/contexts/*/index.ts"),
      ...glob.sync("src/contexts/*/types/*.ts"),
      ...glob.sync("src/data/*.ts"),
      ...glob.sync("src/hooks/*/index.ts"),
      ...glob.sync("src/providers/*/index.tsx"),
      ...glob.sync("src/providers/*/types/*.ts"),
      ...glob.sync("src/types/*.ts"),
      ...glob.sync("src/util/*.ts"),
    ]

    const imports = filenames.map((f) => "./" + f.substring(4, f.lastIndexOf(".")))
    const src = imports.map((i) => `export * from "${i}"`).join("\n")

    writeFileSync("src/lib.ts", src)
  },
  closeBundle: () => {
    unlinkSync("src/lib.ts")
  },
})

export default defineConfig({
  plugins: [temporaryLibEntry(), generateDtsBundle(), react()],
  build: {
    // see https://vitejs.dev/guide/build.html#library-mode
    lib: {
      entry: resolve(__dirname, "src/lib.ts"),
      formats: ["es", "cjs"],
      fileName: (format) => `index.${format}.js`,
    },
    rollupOptions: {
      external,
    },
    outDir: "lib",
  },
})
