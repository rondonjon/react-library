import { ReactNode, useMemo } from "react"
import { locationContext } from "../../contexts/location"
import { LocationContextProps } from "../../contexts/location/types/LocationContextProps"
import { useLocation } from "../../hooks/useLocation"

export const InterceptingLocationProvider = ({ children }: { children: ReactNode }) => {
  const { current } = useLocation()

  const value = useMemo<LocationContextProps>(
    () => ({
      current,
      set: (location: string) => {
        alert(`Location change intercepted: "${location}"`)
      },
    }),
    [current]
  )

  return <locationContext.Provider value={value}>{children}</locationContext.Provider>
}
