import { themeContext } from "../../contexts/theme"
import { ThemeProviderProps } from "./types/ThemeProviderProps"

export const ThemeProvider = ({ children, value }: ThemeProviderProps) => (
  <themeContext.Provider value={value}>{children}</themeContext.Provider>
)
