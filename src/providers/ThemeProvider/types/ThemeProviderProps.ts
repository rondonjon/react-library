import { ReactNode } from "react"
import { Theme } from "../../../types/Theme"

export type ThemeProviderProps = {
  children: ReactNode
  value: Theme
}
