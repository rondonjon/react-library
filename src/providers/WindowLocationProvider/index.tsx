import { useEffect, useMemo, useState } from "react"
import { locationContext } from "../../contexts/location"
import { LocationContextProps } from "../../contexts/location/types/LocationContextProps"
import { WindowLocationProviderProps } from "./types/WindowLocationProviderProps"

export const WindowLocationProvider = ({ children }: WindowLocationProviderProps) => {
  const [location, setLocation] = useState(window.location.href)

  useEffect(() => {
    const handleHashChange = (e: HashChangeEvent) => {
      setLocation(e.newURL)
    }

    const handleHistoryChange = () => {
      setLocation(window.location.href)
    }

    window.addEventListener("hashchange", handleHashChange)
    window.addEventListener("popstate", handleHistoryChange)

    return () => {
      window.removeEventListener("hashchange", handleHashChange)
      window.removeEventListener("popstate", handleHistoryChange)
    }
  }, [])

  const value = useMemo<LocationContextProps>(
    () => ({
      current: location,
      set: (newLocation: string) => {
        const url = new URL(newLocation, location)
        window.location.href = url.toString()
      },
    }),
    [location]
  )

  return <locationContext.Provider value={value}>{children}</locationContext.Provider>
}
