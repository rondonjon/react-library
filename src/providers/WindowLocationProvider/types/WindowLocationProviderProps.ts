import { ReactNode } from "react"

export type WindowLocationProviderProps = {
  children: ReactNode
}
