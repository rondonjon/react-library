import { ChangeEvent, useCallback } from "react"
import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { exp } from "../../util/exp"
import { RadioButtonProps } from "./types/RadioButtonProps"

export const RadioButton = ({ id, isChecked, isDisabled, name, onChange, value }: RadioButtonProps) => {
  const { palette, style } = useTheme()
  const { fontSize, roundness, light, slope } = useThemeStyleCss()
  const background = palette.background.blend(palette.selection, light(0.05, 0.1))

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const { checked: isChecked, value, name } = e.currentTarget
      onChange?.(isChecked, value, name)
    },
    [onChange]
  )

  return (
    <input
      checked={isChecked}
      css={{
        appearance: "none",
        background: background.toRGBA(),
        borderRadius: roundness(style.textScaling * 0.5),
        boxShadow: [
          // outline
          `0 0 0 ${slope(0.15)} ${background.blend(palette.dark, light(0.75)).toRGBA()}`,
          // top shadow
          `inset 0 -${slope(0.1)} 0 0 ${background
            .blend(palette.background, exp(light(0.5), "inc", "fast"))
            .blend(palette.light, exp(light(0.7), "inc", "fast"))
            .toRGBA()}`,
          // bottom shadow
          `inset 0 ${slope(0.1)} 0 0 ${background
            .blend(palette.background, exp(light(0.5), "inc", "fast"))
            .blend(palette.dark, light(0.7))
            .toRGBA()}`,
          // border
          `inset 0 0 0 ${fontSize(0.15)} ${background.toRGBA()}`,
        ].join(","),
        cursor: "pointer",
        display: "inline-block",
        height: fontSize(1),
        position: "relative",
        width: fontSize(1),
        "&::after": {
          background: background.toRGBA(),
          borderRadius: roundness(style.textScaling * 0.5),
          content: "''",
          fontSize: 0,
          height: fontSize(0.6),
          left: fontSize(0.2),
          opacity: 0,
          position: "absolute",
          transform: "scale(0)",
          transformOrigin: "center",
          transition: "all .1s linear",
          top: fontSize(0.2),
          width: fontSize(0.6),
        },
        "&:active:not(:disabled)::after": {
          background: background.blend(palette.selection, 0.5).toRGBA(),
          opacity: 1,
          transform: "scale(0.5)",
        },
        "&:checked:not(:disabled)::after": {
          background: palette.selection.toRGBA(),
          opacity: 1,
          transform: "scale(1.0)",
        },
        "&:checked:active:not(:disabled)::after": {
          background: palette.selection.toRGBA(),
          opacity: 1,
          transform: "scale(0.75)",
        },
        ":disabled": {
          background: palette.disabled.toRGBA(),
          boxShadow: "none",
          cursor: "not-allowed",
          textShadow: undefined,
        },
      }}
      disabled={isDisabled}
      id={id}
      name={name}
      onChange={handleChange}
      type="radio"
      value={value}
    />
  )
}
