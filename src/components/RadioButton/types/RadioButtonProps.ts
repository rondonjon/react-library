export type RadioButtonProps = {
  id?: string
  isChecked: boolean
  isDisabled?: boolean
  name: string
  onChange?: (isChecked: boolean, value: string, name: string) => void
  value: string
}
