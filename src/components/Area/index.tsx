import { useMemo } from "react"
import { themeContext } from "../../contexts/theme"
import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { Theme } from "../../types/Theme"
import { AreaProps } from "./types"

export const Area = ({ children, padding = "m" }: AreaProps) => {
  const theme = useTheme()
  const { fontSize, roundness, slope, spacing } = useThemeStyleCss()

  const borderColor = theme.palette.background.blend(theme.palette.selection, 0.1 + 0.05 * theme.style.light)
  const backgroundColor = theme.palette.background.blend(borderColor, theme.style.fill)
  const darkerColor = theme.palette.background.blend(theme.palette.dark, 0.35 * theme.style.light)

  const contentTheme = useMemo<Theme>(
    () => ({
      ...theme,
      palette: {
        ...theme.palette,
        background: backgroundColor,
      },
    }),
    [backgroundColor, theme]
  )

  const paddingFactor = padding === "s" ? 0.5 : padding === "l" ? 1.5 : 1.0

  return (
    <div
      css={{
        background: backgroundColor.toRGBA(),
        borderRadius: roundness(0.5),
        boxShadow: [
          // top shadow
          `inset 0 ${slope(0.125)} ${darkerColor.toRGBA()}`,
          // border
          `inset 0 0 0 ${fontSize(0.15)} ${borderColor.toRGBA()}`,
        ].join(","),
        display: "grid",
        gap: spacing(paddingFactor, paddingFactor * 0.25),
        gridTemplateRows: "1fr",
        padding: spacing(paddingFactor, paddingFactor * 0.25),
      }}
    >
      <themeContext.Provider value={contentTheme}>{children}</themeContext.Provider>
    </div>
  )
}
