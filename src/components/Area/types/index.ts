import { ReactNode } from "react"

export type AreaProps = {
  children: ReactNode
  padding?: "s" | "m" | "l"
}
