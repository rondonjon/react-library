import { Property } from "csstype"
import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { useUserSelect } from "../../hooks/useUserSelect"
import { MonospaceProps } from "./types/MonospaceProps"

export const Monospace = ({ children, type = "inline" }: MonospaceProps) => {
  const theme = useTheme()
  const { fontSize, spacing } = useThemeStyleCss()
  const userSelect = useUserSelect()

  let Component: "span" | "pre" | "p"
  let display: Property.Display | undefined
  let padding: Property.Padding | undefined
  let unicodeBidi: Property.UnicodeBidi | undefined
  let whiteSpace: Property.WhiteSpace | undefined

  if (type === "inline") {
    Component = "span"
    display = "inline"
  } else {
    display = "block"
    padding = `${spacing(1)} ${spacing(0)}`

    if (type === "preformatted-block") {
      Component = "pre"
      unicodeBidi = "embed"
      whiteSpace = "pre-wrap"
    } else {
      Component = "p"
    }
  }

  return (
    <Component
      css={{
        color: theme.palette.monospace.toRGBA(),
        display,
        fontFamily: theme.fonts.monospace.family,
        fontSize: fontSize(1),
        fontWeight: theme.fonts.monospace.weight,
        lineHeight: 1.25, // unitless, see https://medium.com/@EricMasiello/when-to-use-rems-ems-px-or-whatever-else-in-css-5ae37176af1c
        margin: 0,
        padding,
        textShadow: "none", // overwrite inherited styles
        unicodeBidi,
        userSelect: userSelect === false ? "none" : undefined,
        whiteSpace,
      }}
    >
      {children}
    </Component>
  )
}
