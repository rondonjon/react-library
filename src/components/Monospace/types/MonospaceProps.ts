import { ReactNode } from "react"

export type MonospaceProps = {
  children: ReactNode
  type?: "inline" | "block" | "preformatted-block"
}
