import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { useUserSelect } from "../../hooks/useUserSelect"
import { LabelProps } from "./types/LabelProps"

export const Label = ({ children, for: forProp }: LabelProps) => {
  const theme = useTheme()
  const { fontSize, light, slope } = useThemeStyleCss()
  const userSelect = useUserSelect()

  return (
    <label
      css={{
        color: theme.palette.paragraph.toRGBA(),
        display: "inline",
        fontFamily: theme.fonts.paragraph.family,
        fontSize: fontSize(1.0),
        fontWeight: theme.fonts.paragraph.weight,
        margin: 0,
        padding: 0,
        textShadow: `0 ${slope(0.05)} ${fontSize(0.1 * theme.style.slope)} ${theme.palette.background
          .blend(theme.palette.paragraph, light(1))
          .toRGBA()}`,
        userSelect: userSelect === false ? "none" : undefined,
      }}
      htmlFor={forProp}
    >
      {children}
    </label>
  )
}
