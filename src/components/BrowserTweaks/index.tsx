import { Global } from "@emotion/react"
import { Property } from "csstype"
import { ReactNode, useEffect } from "react"
import { BrowserTweaksProps } from "./types/BrowserTweaksProps"

export const BrowserTweaks = ({ allowDragging, allowOverScrolling, themeColor }: BrowserTweaksProps) => {
  const children: ReactNode[] = []

  // Apply `allowDragging`

  useEffect(() => {
    if (allowDragging === false) {
      const cancelEvent = (e: DragEvent) => {
        e.preventDefault()
        e.stopPropagation()
      }

      window.addEventListener("dragstart", cancelEvent)

      return () => {
        window.removeEventListener("dragstart", cancelEvent)
      }
    }
  })

  // Apply `allowOverScrolling`

  const overscrollBehaviorY: Property.OverscrollBehaviorY =
    allowOverScrolling === true ? "contain" : allowOverScrolling === false ? "none" : "auto"

  children.push(
    // "official" solution, but as os 2022 not available in iOS,
    // see https://www.bram.us/2016/05/02/prevent-overscroll-bounce-in-ios-mobilesafari-pure-css/
    <Global
      key="overscrollBehavior"
      styles={{
        html: {
          overscrollBehaviorY,
        },
        body: {
          overscrollBehaviorY,
        },
      }}
    />
  )

  // Apply `themeColor`

  useEffect(() => {
    if (themeColor) {
      const meta = document.createElement("meta")

      meta.name = "theme-color"
      meta.content = themeColor.toHex()

      document.head.appendChild(meta)

      return () => {
        meta.remove()
      }
    }
  }, [themeColor])

  if (themeColor) {
    children.push(
      <Global
        key="themeColor"
        styles={{
          html: {
            background: themeColor.toHex(),
          },
        }}
      />
    )
  }

  // Return global styles

  return <>{children}</>
}
