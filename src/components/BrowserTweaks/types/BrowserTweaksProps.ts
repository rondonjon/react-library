import { Color } from "../../../types/Color"

export type BrowserTweaksProps = {
  /**
   * Set to `false` to prevent dragging (and thereby "ghost" images) on the entire page.
   * See https://stackoverflow.com/questions/7439042/css-js-to-prevent-dragging-of-ghost-image
   */
  allowDragging?: boolean

  /**
   * Set to enable/disable overscrolling of the page.
   * See https://developer.chrome.com/blog/overscroll-behavior/#disabling-overscroll-glow-and-rubberbanding-effects
   */
  allowOverScrolling?: boolean

  /**
   * Set to apply a theme color for native controls in browsers that support it.
   * See https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta/name/theme-color
   */
  themeColor?: Color
}
