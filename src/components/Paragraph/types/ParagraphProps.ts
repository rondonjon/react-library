import { ReactNode } from "react"

export type ParagraphProps = {
  children: ReactNode
  size?: "s" | "m" | "l"
  textAlign?: "left" | "center" | "right"
}
