import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { useUserSelect } from "../../hooks/useUserSelect"
import { ParagraphProps } from "./types/ParagraphProps"

export const Paragraph = ({ children, size = "m", textAlign = "left" }: ParagraphProps) => {
  const theme = useTheme()
  const { fontSize, light, slope, spacing } = useThemeStyleCss()
  const userSelect = useUserSelect()

  return (
    <p
      css={{
        color: theme.palette.paragraph.toRGBA(),
        fontFamily: theme.fonts.paragraph.family,
        fontSize: fontSize(size === "s" ? 0.5 : size === "l" ? 1.5 : 1.0),
        fontWeight: theme.fonts.paragraph.weight,
        lineHeight: 1.25, // unitless, see https://medium.com/@EricMasiello/when-to-use-rems-ems-px-or-whatever-else-in-css-5ae37176af1c
        margin: 0,
        padding: `${spacing(1)} ${spacing(0)}`,
        textShadow: `0 ${slope(0.05)} ${fontSize(0.1 * theme.style.slope)} ${theme.palette.background
          .blend(theme.palette.paragraph, light(1))
          .toRGBA()}`,
        textAlign,
        userSelect: userSelect === false ? "none" : undefined,
      }}
    >
      {children}
    </p>
  )
}
