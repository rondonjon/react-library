import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { useUserSelect } from "../../hooks/useUserSelect"
import { HeadlineProps } from "./types"

const scaling: Record<HeadlineProps["level"], number> = {
  1: 3.0,
  2: 2.6,
  3: 2.2,
  4: 1.8,
  5: 1.5,
  6: 1.2,
}

export const Headline = ({ level, textAlign, children }: HeadlineProps) => {
  const theme = useTheme()
  const { fontSize, light, slope, spacing } = useThemeStyleCss()
  const Component = `h${level}` as const
  const userSelect = useUserSelect()

  return (
    <Component
      css={{
        // cursor: default;
        color: theme.palette.headline.toRGBA(),
        fontFamily: theme.fonts.headline.family,
        fontSize: fontSize(scaling[level]),
        fontWeight: theme.fonts.headline.weight,
        letterSpacing: "0.025em" /* intentionally em, not rem */,
        lineHeight: 1.25, // unitless, see https://medium.com/@EricMasiello/when-to-use-rems-ems-px-or-whatever-else-in-css-5ae37176af1c
        margin: 0,
        padding: `${spacing(1)} ${spacing(0)}`,
        textShadow: `0 ${slope(0.05)} ${fontSize(0.1 * theme.style.slope)} ${theme.palette.background
          .blend(theme.palette.headline, light(1))
          .toRGBA()}`,
        textAlign,
        textTransform: "uppercase",
        userSelect: userSelect === false ? "none" : undefined,
      }}
    >
      {children}
    </Component>
  )
}
