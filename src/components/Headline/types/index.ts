import { ReactNode } from "react"

export type HeadlineProps = {
  children: ReactNode
  level: 1 | 2 | 3 | 4 | 5 | 6
  textAlign?: "left" | "center" | "right"
}
