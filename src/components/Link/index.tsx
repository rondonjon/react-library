import { MouseEvent, useCallback } from "react"
import { useLocation } from "../../hooks/useLocation"
import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { LinkProps } from "./types/LinkProps"

export const Link = ({ children, href, target = "_self", title }: LinkProps) => {
  const theme = useTheme()
  const { fontSize, slope, light } = useThemeStyleCss()
  const { set: setLocation } = useLocation()

  const handleClick = useCallback(
    (e: MouseEvent) => {
      if (target === "_self") {
        // Cancel default action and redirect navigation to location context
        e.preventDefault()
        e.stopPropagation()
        setLocation(href)
      }
    },
    [href, setLocation, target]
  )

  return (
    <a
      href={href}
      onClick={handleClick}
      target={target}
      title={title}
      // https://security.stackexchange.com/questions/241559/using-rel-noreferer-without-rel-noopener-or-without-target-blank
      rel={target === "_blank" ? "noreferrer" : undefined}
      css={{
        color: `${theme.palette.link.toRGBA()} !important`,
        textDecoration: "none !important",
        ":hover": {
          textDecoration: "underline !important",
        },
        textShadow: `0 ${slope(0.05)} ${fontSize(0.1 * theme.style.slope)} ${theme.palette.background
          .blend(theme.palette.link, light(1))
          .toRGBA()}`,
        touchAction: "manipulation", // skip 300 ms delay on click
      }}
    >
      {children}
    </a>
  )
}
