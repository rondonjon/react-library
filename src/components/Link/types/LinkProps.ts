import { HTMLAttributeAnchorTarget, ReactNode } from "react"

export type LinkProps = {
  children: ReactNode
  href: string
  target?: HTMLAttributeAnchorTarget
  title?: string
}
