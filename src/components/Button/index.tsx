import { MouseEvent, useCallback } from "react"
import { ButtonProps } from "./types/ButtonProps"
import { useLocation } from "../../hooks/useLocation"
import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { exp } from "../../util/exp"

export const Button = (props: ButtonProps) => {
  const { fonts, palette, style } = useTheme()
  const { fontSize, light, roundness, slope, spacing } = useThemeStyleCss()
  const { label, type, isDisabled } = props
  const onClick = "onClick" in props ? props.onClick : undefined
  const href = "href" in props ? props.href : undefined
  const { set: setLocation } = useLocation()

  const handleClick = useCallback(
    (e: MouseEvent) => {
      if (isDisabled) {
        return
      } else if (href) {
        setLocation(href)
      } else if (onClick) {
        onClick(e)
      }
    },
    [isDisabled, href, setLocation, onClick]
  )

  const borderColor = isDisabled
    ? palette.disabled
    : type === "danger"
    ? palette.error
    : type === "primary"
    ? palette.emphasis
    : palette.background.blend(palette.selection, light(0, 0.15))

  const fillColor = palette.background.blend(borderColor, style.fill)
  const paddingX = spacing(0.35, style.roundness * 0.1 + style.textScaling * 0.35)
  const paddingY = spacing(0.25, style.roundness * 0.075 + style.textScaling * 0.25)
  const activeTextShiftY = spacing(0.1)

  return (
    <button
      css={{
        alignItems: "center",
        backgroundColor: fillColor.toRGBA(),
        borderColor: fillColor.toRGBA(),
        borderRadius: roundness(style.textScaling + style.spacing * 0.5),
        borderWidth: 0,
        boxSizing: "border-box",
        boxShadow: [
          // outline
          `0 0 0 ${slope(0.15)} ${palette.background.blend(palette.dark, light(0.75)).toRGBA()}`,
          // top shadow
          `inset 0 ${slope(0.1)} 0 0 ${borderColor
            .blend(palette.background, exp(light(0.5), "inc", "fast"))
            .blend(palette.light, exp(light(0.7), "inc", "fast"))
            .toRGBA()}`,
          // bottom shadow
          `inset 0 -${slope(0.1)} 0 0 ${borderColor.blend(palette.dark, light(0.7)).toRGBA()}`,
          // border
          `inset 0 0 0 ${fontSize(0.15)} ${borderColor.toRGBA()}`,
        ].join(","),
        color: palette.selection.toRGBA(),
        cursor: "pointer",
        display: "inline-block",
        fontFamily: fonts.headline.family,
        fontSize: fontSize(1),
        fontWeight: fonts.headline.weight,
        justifyContent: "center",
        letterSpacing: "0.02em",
        lineHeight: 1, // unitless, see https://medium.com/@EricMasiello/when-to-use-rems-ems-px-or-whatever-else-in-css-5ae37176af1c
        maxWidth: "100%",
        overflow: "hidden",
        padding: `${paddingY} ${paddingX}`,
        textOverflow: "ellipsis",
        textTransform: "uppercase",
        touchAction: "manipulation", // skip 300 ms delay on click
        transition: "all .1s linear",
        userSelect: "none",
        whiteSpace: "nowrap",
        ":active:not(:disabled)": {
          boxShadow: [
            // outline
            `0 0 0 ${slope(0.15)} ${palette.background.blend(palette.dark, light(0.75)).toRGBA()}`,
            // top shadow
            `inset 0 ${slope(0.25)} 0 0 ${borderColor.blend(palette.dark, 0.2 + light(0.1)).toRGBA()}`,
            // bottom shadow
            `inset 0 -${slope(0.1)} 0 0 ${borderColor.toRGBA()}`,
            // border
            `inset 0 0 0 ${fontSize(0.15)} ${borderColor.toRGBA()}`,
          ].join(","),
          color: palette.selection.blend(fillColor, 0.3 - light(0.3)).toRGBA(),
          paddingTop: `calc(${paddingY} + ${activeTextShiftY})`,
          paddingBottom: `calc(${paddingY} - ${activeTextShiftY})`,
        },
        ":disabled": {
          boxShadow: "none",
          color: palette.selection.blend(fillColor, exp(light(0.3), "inc", "fast")).toRGBA(),
          cursor: "not-allowed",
          textShadow: undefined,
        },
      }}
      disabled={isDisabled}
      onClick={handleClick}
    >
      {label}
    </button>
  )
}
