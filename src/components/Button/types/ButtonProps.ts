import { MouseEvent } from "react"

export type ButtonProps = {
  type?: "primary" | "secondary" | "danger"
  isDisabled?: boolean
  label: string
} & (
  | {
      href?: never
      onClick: (e: MouseEvent) => void
    }
  | {
      href: string
      onClick?: never
    }
)
