export type CheckboxProps = {
  id?: string
  isDisabled?: boolean
  name: string
  onChange?: (value: boolean, name: string) => void
  value: boolean
}
