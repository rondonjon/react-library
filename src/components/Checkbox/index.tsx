import { ChangeEvent, useCallback } from "react"
import { useTheme } from "../../hooks/useTheme"
import { useThemeStyleCss } from "../../hooks/useThemeStyleCss"
import { Color } from "../../types/Color"
import { exp } from "../../util/exp"
import { CheckboxProps } from "./types/CheckboxProps"

const crossImage = (fill: Color) =>
  `url('data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 7 7"><path transform="rotate (45, 3.5, 3.5)" d="M 1 3 L 3 3 L 3 1 L 4 1 L 4 3 L 6 3 L 6 4 L 4 4 L 4 6 L 3 6 L 3 4 L 1 4 Z" fill="${fill.toRGBA()}" /></svg>')`

export const Checkbox = ({ id, isDisabled, name, onChange, value }: CheckboxProps) => {
  const { palette, style } = useTheme()
  const { fontSize, roundness, light, slope } = useThemeStyleCss()
  const background = palette.background.blend(palette.selection, light(0.05, 0.1))

  const handleChange = useCallback(
    (e: ChangeEvent<HTMLInputElement>) => {
      const { checked: value, name } = e.currentTarget
      onChange?.(value, name)
    },
    [onChange]
  )

  return (
    <input
      checked={value}
      css={{
        appearance: "none",
        background: background.toRGBA(),
        borderRadius: roundness(style.textScaling * 0.125),
        boxShadow: [
          // outline
          `0 0 0 ${slope(0.15)} ${background.blend(palette.dark, light(0.75)).toRGBA()}`,
          // top shadow
          `inset 0 -${slope(0.1)} 0 0 ${background
            .blend(palette.background, exp(light(0.5), "inc", "fast"))
            .blend(palette.light, exp(light(0.7), "inc", "fast"))
            .toRGBA()}`,
          // bottom shadow
          `inset 0 ${slope(0.1)} 0 0 ${background
            .blend(palette.background, exp(light(0.5), "inc", "fast"))
            .blend(palette.dark, light(0.7))
            .toRGBA()}`,
          // border
          `inset 0 0 0 ${fontSize(0.15)} ${background.toRGBA()}`,
        ].join(","),
        cursor: "pointer",
        display: "inline-block",
        height: fontSize(1),
        position: "relative",
        width: fontSize(1),
        "&::after": {
          backgroundImage: crossImage(palette.selection),
          backgroundRepeat: "no-repeat",
          backgroundSize: `${fontSize(1)} ${fontSize(1)}`,
          content: "''",
          fontSize: 0,
          height: fontSize(1),
          left: 0,
          opacity: 0,
          position: "absolute",
          transform: "scale(0)",
          transformOrigin: "center",
          transition: "all .1s linear",
          top: 0,
          width: fontSize(1),
        },
        "&:active:not(:disabled)::after": {
          backgroundImage: crossImage(background.blend(palette.selection, 0.5)),
          opacity: 1,
          transform: "scale(0.75)",
        },
        "&:checked:not(:disabled)::after": {
          backgroundImage: crossImage(palette.selection),
          opacity: 1,
          transform: "scale(1.0)",
        },
        "&:checked:active:not(:disabled)::after": {
          backgroundImage: crossImage(palette.selection),
          opacity: 1,
          transform: "scale(0.9)",
        },
        ":disabled": {
          background: palette.disabled.toRGBA(),
          boxShadow: "none",
          cursor: "not-allowed",
          textShadow: undefined,
        },
      }}
      disabled={isDisabled}
      id={id}
      name={name}
      onChange={handleChange}
      type="checkbox"
    />
  )
}
