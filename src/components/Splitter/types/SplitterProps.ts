import { Property } from "csstype"
import { Dispatch, ReactNode, SetStateAction } from "react"
import { Orientation } from "./Orientation"

export type SplitterProps = {
  orientation: Orientation
  a: ReactNode
  b: ReactNode
  position: [number, Dispatch<SetStateAction<number>>]
  width?: Property.Width
  height?: Property.Height
}
