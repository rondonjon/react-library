export type Layout = {
  left: number
  top: number
  width: number
  height: number
}
