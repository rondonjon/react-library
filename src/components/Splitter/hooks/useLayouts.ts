import { useMemo } from "react"
import { useTheme } from "../../../hooks/useTheme"
import { even } from "../../../util/even"
import { Layout } from "../types/Layout"
import { Orientation } from "../types/Orientation"

export const useLayouts = (
  width: number,
  height: number,
  orientation: Orientation,
  position: number
): {
  a: Layout
  bar: Layout
  b: Layout
} => {
  const { spacing } = useTheme().style

  return useMemo(() => {
    const allSize = Math.floor(orientation === "horizontal" ? width : height)
    const barSize = (Math.floor(10 + spacing * 10) / 2) * 2
    const aSize = even(position * allSize - barSize)
    const bSize = allSize - barSize - aSize

    return orientation === "horizontal"
      ? {
          a: {
            left: 0,
            top: 0,
            width: aSize,
            height,
          },
          bar: {
            left: aSize,
            top: 0,
            width: barSize,
            height,
          },
          b: {
            left: aSize + barSize,
            top: 0,
            width: bSize,
            height,
          },
        }
      : {
          a: {
            left: 0,
            top: 0,
            width,
            height: aSize,
          },
          bar: {
            left: 0,
            top: aSize,
            width,
            height: barSize,
          },
          b: {
            left: 0,
            top: aSize + barSize,
            width,
            height: bSize,
          },
        }
  }, [width, height, orientation, position, spacing])
}
