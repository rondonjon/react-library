import { useCallback, useState } from "react"

type Point = {
  x: number
  y: number
}

export const useDragDrop = () => {
  const [diff, setDiff] = useState<Point | undefined>()

  const handleDragStart = useCallback((e: React.DragEvent) => {
    e.stopPropagation()
    e.preventDefault()
  }, [])

  const handlePointerDown = useCallback((e: React.PointerEvent) => {
    e.stopPropagation() // prevent text selection
    e.preventDefault()

    const start: Point = {
      x: e.screenX,
      y: e.screenY,
    }

    setDiff({ x: 0, y: 0 })

    const handlePointerMove = (e: MouseEvent) => {
      setDiff({
        x: start.x - e.screenX,
        y: start.y - e.screenY,
      })
    }

    const handlePointerUp = () => {
      setDiff(undefined)
      window.removeEventListener("pointermove", handlePointerMove)
      window.removeEventListener("pointerup", handlePointerUp)
    }

    window.addEventListener("pointermove", handlePointerMove)
    window.addEventListener("pointerup", handlePointerUp)
  }, [])

  return {
    handleDragStart,
    handlePointerDown,
    diff,
  }
}
