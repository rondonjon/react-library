import { useEffect, useRef } from "react"
import { useClientRect } from "../../hooks/useClientRect"
import { clamp } from "../../util/clamp"
import { Bar } from "./components/Bar"
import { Side } from "./components/Side"
import { useDragDrop } from "./hooks/useDragDrop"
import { useLayouts } from "./hooks/useLayouts"
import { SplitterProps } from "./types/SplitterProps"

export const Splitter = ({
  width: allWidth = "100%",
  height: allHeight = "100%",
  a,
  b,
  orientation,
  position: [position, setPosition],
}: SplitterProps) => {
  const { ref, width = 0, height = 0 } = useClientRect()
  const { handleDragStart, handlePointerDown, diff } = useDragDrop()
  const startPosition = useRef<number | undefined>()

  useEffect(() => {
    if (diff) {
      if (startPosition.current === undefined) {
        startPosition.current = position
      }

      if (width === 0 || height === 0) {
        return
      }

      const posDiff = orientation === "horizontal" ? diff.x / width : diff.y / height
      setPosition(clamp(startPosition.current - posDiff, 0.1, 0.9))
    } else {
      startPosition.current = undefined
    }
  }, [diff, height, orientation, position, setPosition, width])

  const layout = useLayouts(width, height, orientation, position)

  return (
    <div
      css={{
        fontSize: 0,
        height: allHeight,
        overflow: "hidden",
        position: "relative",
        width: allWidth,
      }}
      ref={ref}
    >
      <Side {...layout.a}>{a}</Side>
      <Bar {...layout.bar} onDragStart={handleDragStart} onPointerDown={handlePointerDown} splitterOrientation={orientation} />
      <Side {...layout.b}>{b}</Side>
    </div>
  )
}
