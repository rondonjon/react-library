import { SideProps } from "./types/SideProps"

export const Side = ({ children, top, left, height, width }: SideProps) => (
  <div
    css={{
      maxWidth: "100%",
      maxHeight: "100%",
      overflow: "auto",
      position: "absolute",
    }}
    style={{
      height,
      left,
      top,
      width,
    }}
  >
    {children}
  </div>
)
