import { ReactNode } from "react"
import { Layout } from "../../../types/Layout"

export type SideProps = Layout & {
  children: ReactNode
}
