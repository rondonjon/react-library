import { useTheme } from "../../../../hooks/useTheme"
import { useThemeStyleCss } from "../../../../hooks/useThemeStyleCss"
import { exp } from "../../../../util/exp"
import { BarProps } from "./types/BarProps"

export const Bar = ({ height, left, onDragStart, onPointerDown, splitterOrientation, top, width }: BarProps) => {
  const { palette } = useTheme()
  const { fontSize, light, roundness, slope } = useThemeStyleCss()
  const background = palette.background.blend(palette.selection, light(0.05, 0.1))
  const padding = Math.round((splitterOrientation === "horizontal" ? width : height) * 0.2)

  return (
    <div
      css={{
        position: "absolute",
        touchAction: "none",
      }}
      onDragStart={onDragStart}
      onPointerDown={onPointerDown}
      style={{
        cursor: splitterOrientation === "vertical" ? "ns-resize" : "ew-resize",
        height,
        left,
        padding,
        top,
        width,
      }}
    >
      <div
        css={{
          background: background.toRGBA(),
          borderRadius: roundness(0.5),
          boxShadow: [
            // outline
            `0 0 0 ${slope(0.15)} ${background.blend(palette.dark, light(0.75)).toRGBA()}`,
            // top shadow
            `inset 0 ${slope(0.1)} 0 0 ${background
              .blend(palette.background, exp(light(0.5), "inc", "fast"))
              .blend(palette.light, exp(light(0.7), "inc", "fast"))
              .toRGBA()}`,
            // bottom shadow
            `inset 0 -${slope(0.1)} 0 0 ${background
              .blend(palette.background, exp(light(0.5), "inc", "fast"))
              .blend(palette.dark, light(0.7))
              .toRGBA()}`,
            // border
            `inset 0 0 0 ${fontSize(0.15)} ${background.toRGBA()}`,
          ].join(","),
          height: "100%",
          width: "100%",
        }}
      />
    </div>
  )
}
