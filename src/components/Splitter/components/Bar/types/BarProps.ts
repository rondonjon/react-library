import React from "react"
import { Layout } from "../../../types/Layout"
import { Orientation } from "../../../types/Orientation"

export type BarProps = Layout & {
  onDragStart: (e: React.DragEvent) => void
  onPointerDown: (e: React.PointerEvent) => void
  splitterOrientation: Orientation
}
