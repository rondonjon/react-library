import { useEffect, useMemo, useState } from "react"

/**
 * Monitors and returns the position and the size of an element.
 *
 * Returns:
 * - a callback ref (https://reactjs.org/docs/hooks-faq.html#how-can-i-measure-a-dom-node)
 * which must be assigned to the `ref` of the JSX element that is to be watched
 * - a the last known size of that element
 */
export const useClientRect = () => {
  const [element, setElement] = useState<Element | null>(null)
  const [rect, setRect] = useState<DOMRectReadOnly | undefined>()

  useEffect(() => {
    if (element === null) {
      return
    }

    const observer = new ResizeObserver((entries: ResizeObserverEntry[]) => {
      if (entries.length) {
        setRect(entries[0].contentRect)
      }
    })

    observer.observe(element)

    return () => {
      observer.unobserve(element)
    }
  }, [element])

  return useMemo(
    () => ({
      ref: setElement,
      top: rect?.top,
      left: rect?.left,
      width: rect?.width,
      height: rect?.height,
    }),
    [rect]
  )
}
