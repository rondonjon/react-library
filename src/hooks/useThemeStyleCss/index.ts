import { Theme } from "../../types/Theme"
import { useTheme } from "../useTheme"

type Unit = Theme["style"]["nonTextUnits"]

const DEFAULT_FONT_SIZE_PX = 16

const toCss = (proportional: number, unit: Unit) => {
  if (unit === "text-proportional") {
    return `${proportional}rem`
  } else {
    return `${Math.round(proportional * DEFAULT_FONT_SIZE_PX)}px`
  }
}

export const useThemeStyleCss = () => {
  const { light, roundness, slope, spacing, textScaling, nonTextUnits } = useTheme().style

  return {
    light: (factor: number, offset = 0) => factor * light + offset,
    slope: (factor: number, offset = 0) => toCss(factor * slope + offset, nonTextUnits),
    fontSize: (factor: number, offset = 0) => toCss(factor * textScaling + offset, "text-proportional"),
    roundness: (factor: number, offset = 0) => toCss(factor * roundness + offset, nonTextUnits),
    spacing: (factor: number, offset = 0) => toCss(factor * spacing + offset, nonTextUnits),
  }
}
