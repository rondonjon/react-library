import { useContext } from "react"
import { locationContext } from "../../contexts/location"

export const useLocation = () => useContext(locationContext)
