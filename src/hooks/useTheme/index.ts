import { useContext } from "react"
import { themeContext } from "../../contexts/theme"

export const useTheme = () => useContext(themeContext)
