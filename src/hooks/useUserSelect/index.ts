import { useContext } from "react"
import { userSelectContext } from "../../contexts/userSelect"

export const useUserSelect = () => useContext(userSelectContext)
