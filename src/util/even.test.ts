import test from "ava"
import { even } from "./even"

test("even()", (t) => {
  t.is(even(2), 2, "even(2) should be even")
  t.is(even(1), 2, "even(1) should return 2")
  t.is(even(2.1), 2, "even(2.1) should return 2")
  t.is(even(3.9), 4, "even(3.9) should return 4")
})
