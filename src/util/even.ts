export const even = (n: number) => Math.round(Math.round(n / 2) * 2)
