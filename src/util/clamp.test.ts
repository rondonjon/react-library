import test from "ava"
import { clamp } from "./clamp"

test("clamp()", (t) => {
  t.is(clamp(+0.5, 0.0, 1.0), 0.5, "within range")
  t.is(clamp(-0.5, 0.0, 1.0), 0.0, "clamps lower")
  t.is(clamp(+1.5, 0.0, 1.0), 1.0, "clamps higher")
})
