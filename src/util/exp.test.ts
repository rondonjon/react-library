import test from "ava"
import { exp } from "./exp"

test("exp(v, incline, fast)", (t) => {
  const e = exp(0.25, "inc", "fast")
  const l = 0.25
  t.true(e >= 0.0 && e <= 1.0, "result in range")
  t.true(e > l, `result (${e}) greater than with linear incline (${l})`)
})

test("exp(v, incline, slow)", (t) => {
  const e = exp(0.25, "inc", "slow")
  const l = 0.25
  t.true(e >= 0.0 && e <= 1.0, "result in range")
  t.true(e < l, `result (${e}) less than with linear incline (${l})`)
})

test("exp(v, decline, fast)", (t) => {
  const e = exp(0.25, "dec", "fast")
  const l = 0.75
  t.true(e >= 0.0 && e <= 1.0, "result in range")
  t.true(e < l, `result (${e}) less than with linear decline (${l})`)
})

test("exp(v, decline, slow)", (t) => {
  const e = exp(0.25, "dec", "slow")
  const l = 0.75
  t.true(e >= 0.0 && e <= 1.0, "result in range")
  t.true(e > l, `result (${e}) greater than with linear decline (${l})`)
})
