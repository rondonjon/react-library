import { clamp } from "./clamp"

/**
 * Clamps a given number to the 0.0 to 1.0 "linear" range and returns
 * the corresponding value from a range that inclines (0.0 -> 1.0)
 * or declines (1.0 -> 0.0) fast (overproportionally transformed output at input 0.5)
 * or slow (underproportionally transformed output at input 0.5).
 */
export const exp = (input: number, func: "inc" | "dec", progression: "fast" | "slow") => {
  const v = clamp(input, 0.0, 1.0)
  let quadrant: number
  let offset: number

  if (func === "inc") {
    if (progression === "fast") {
      quadrant = 0
      offset = 0
    } else {
      quadrant = 3
      offset = 1.0
    }
  } else {
    if (progression === "fast") {
      quadrant = 2
      offset = 1.0
    } else {
      quadrant = 1
      offset = 0
    }
  }

  return Math.sin(((quadrant + v) / 4) * (Math.PI * 2)) + offset
}
