import { Theme } from "../types/Theme"

declare module "styled-components" {
  export interface DefaultTheme {
    id: Theme
  }
}
