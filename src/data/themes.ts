import { Theme } from "../types/Theme"
import { colors } from "./colors"

export const defaultLightTheme: Theme = {
  fonts: {
    paragraph: {
      family: "serif",
      weight: 300,
    },
    headline: {
      family: "sans-serif",
      weight: 600,
    },
    monospace: {
      family: "monospace",
      weight: 300,
    },
  },
  palette: {
    background: colors.beige,
    light: colors.white,
    dark: colors.navyblue.blend(colors.black, 0.2),
    paragraph: colors.bluegrey,
    headline: colors.lavender,
    monospace: colors.lushgreen.blend(colors.beige, 0.25),
    link: colors.cherry,
    selection: colors.black,
    emphasis: colors.blue.blend(colors.beige, 0.8),
    error: colors.darkred.blend(colors.beige, 0.7),
    disabled: colors.grey,
    success: colors.lushgreen,
    warning: colors.orange,
  },
  style: {
    fill: 1.0, // backward-compatible
    slope: 0.5,
    light: 0.5,
    roundness: 0.5,
    spacing: 0.5,
    textScaling: 1.0,
    nonTextUnits: "absolute",
  },
}

export const defaultDarkTheme: Theme = {
  ...defaultLightTheme,
  palette: {
    background: colors.navyblue,
    light: colors.white,
    dark: colors.black,
    paragraph: colors.beige,
    headline: colors.lavender.blend(colors.beige, 0.25),
    monospace: colors.lushgreen.blend(colors.beige, 0.25),
    link: colors.pink,
    selection: colors.white,
    disabled: colors.darkgrey,
    emphasis: colors.skyblue,
    error: colors.red,
    success: colors.lushgreen,
    warning: colors.orange,
  },
}
