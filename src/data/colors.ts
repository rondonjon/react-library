import { Color } from "../types/Color"

// eslint-disable-next-line @typescript-eslint/no-namespace
export namespace colors {
  export const anthracite = new Color(0.098, 0.208, 0.227)
  export const black = new Color(0, 0, 0)
  export const lightgrey = new Color(0.85, 0.85, 0.85)
  export const grey = new Color(0.5, 0.5, 0.5)
  export const white = new Color(1, 1, 1)
  export const navyblue = new Color(0.17, 0.2, 0.35)
  export const green = new Color(0.2, 0.42, 0.32)
  export const purple = new Color(0.46, 0.2, 0.47)
  export const skyblue = new Color(0, 0.56, 0.94)
  export const lushgreen = new Color(0, 0.627, 0)
  export const orange = new Color(0.87, 0.66, 0.03)
  export const red = new Color(0.7, 0, 0)
  export const darkred = new Color(0.56, 0, 0)
  export const darkgrey = new Color(0.25, 0.25, 0.25)
  export const bluegrey = new Color(0.26, 0.26, 0.352)
  export const lavender = new Color(0.564, 0.4, 0.529)
  export const beige = new Color(0.902, 0.882, 0.851)
  export const pink = new Color(0.992, 0.501, 0.627)
  export const blue = new Color(0.001, 0.425, 0.981)
  export const cherry = new Color(0.804, 0.102, 0.22)
}
