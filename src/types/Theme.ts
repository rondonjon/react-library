import { Color } from "./Color"

type Font = {
  family: string
  weight: 100 | 200 | 300 | 400 | 500 | 600 | 700 | 800 | 900
}

export type Theme = {
  fonts: {
    paragraph: Font
    headline: Font
    monospace: Font
  }
  palette: {
    background: Color

    light: Color
    dark: Color

    paragraph: Color
    headline: Color
    monospace: Color
    link: Color

    selection: Color
    emphasis: Color
    warning: Color
    error: Color
    success: Color
    disabled: Color
  }
  style: {
    fill: number // range: 0.0-1.0
    slope: number // range: 0.0-1.0
    light: number // range: 0.0-1.0
    roundness: number // range: 0.0-1.0
    spacing: number // range: 0.0-1.0
    textScaling: number // range: positive fractional; 1.0 to keep root font size (usually 1rem or 16px unless overwritten by code or browser preferences)
    nonTextUnits:
      | "text-proportional" // spacings/borders in rem (as in Bootstrap, change of root font-size scales everything)
      | "absolute" // spacings/borders in px (as in iOS, change of root "font size" literally only applies to fonts)
  }
}
