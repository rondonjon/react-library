import test from "ava"
import { Color } from "./Color"

test("Color", (t) => {
  const color = new Color(1.0, 0.5, 0.25, 1.0)
  const white = new Color(1.0, 1.0, 1.0, 1.0)
  const black = new Color(0.0, 0.0, 0.0, 1.0)

  t.is(color.r, 1.0, ".r")
  t.is(color.g, 0.5, ".g")
  t.is(color.b, 0.25, ".b")
  t.is(color.a, 1.0, ".a")
  t.is(color.toHex(), "#ff8040", ".toHex()")
  t.is(color.toHexA(), "#ff8040ff", ".toHexA()")
  t.is(color.toRGB(), "rgb(255,128,64)", ".toRGB()")
  t.is(color.toRGBA(), "rgba(255,128,64,1)", ".toRGBA()")
  t.deepEqual(color.invert(), new Color(0.0, 0.5, 0.75, 1.0), ".invert()")
  t.deepEqual(color.add(new Color(1.0, 1.0, 1.0, 1.0), 0.1), new Color(1.0, 0.6, 0.35, 1.0), ".add() #1")
  t.deepEqual(color.add(new Color(1.0, 1.0, 1.0, 0.0), 0.1), new Color(1.0, 0.6, 0.35, 1.0), ".add() #2")
  t.deepEqual(color.add(new Color(0.0, 0.0, 0.0, 1.0), 0.1), color, ".add() #3")
  t.deepEqual(color.sub(new Color(1.0, 1.0, 1.0, 1.0), 0.1), new Color(0.9, 0.4, 0.15, 1.0), ".sub()")
  t.deepEqual(color.multiply(0.9), new Color(0.9, 0.45, 0.225, 0.9), ".multiply()")
  t.deepEqual(white.blend(black, 0.3), new Color(0.7, 0.7, 0.7, 1.0), ".blend() #1")
  t.deepEqual(color.blend(black, 0.3), new Color(0.7, 0.35, 0.175, 1.0), ".blend() #2")
})
