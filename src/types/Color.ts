import { clamp } from "../util/clamp"

export class Color {
  constructor(private _r: number, private _g: number, private _b: number, private _a = 1.0) {
    Color.assertWithin(_r, 0.0, 1.0)
    Color.assertWithin(_g, 0.0, 1.0)
    Color.assertWithin(_b, 0.0, 1.0)
    Color.assertWithin(_a, 0.0, 1.0)
  }

  private static assertWithin(v: number, min: number, max: number) {
    if (Number.isNaN(v) || v < min || v > max) {
      throw new Error(`Number out of range (${min}, ${max}): ${v}`)
    }
  }

  multiply(factor: number) {
    return new Color(
      clamp(this._r * factor, 0.0, 1.0),
      clamp(this._g * factor, 0.0, 1.0),
      clamp(this._b * factor, 0.0, 1.0),
      clamp(this._a * factor, 0.0, 1.0)
    )
  }

  add(other: Color, factor: number) {
    return new Color(
      clamp(this._r + factor * other._r, 0.0, 1.0),
      clamp(this._g + factor * other._g, 0.0, 1.0),
      clamp(this._b + factor * other._b, 0.0, 1.0),
      this._a
    )
  }

  sub(other: Color, factor: number) {
    return this.add(other, -factor)
  }

  blend(other: Color, factor: number) {
    Color.assertWithin(factor, 0.0, 1.0)
    return new Color(
      this._r * (1.0 - factor) + other._r * factor,
      this._g * (1.0 - factor) + other._g * factor,
      this._b * (1.0 - factor) + other._b * factor,
      this._a * (1.0 - factor) + other._a * factor
    )
  }

  invert() {
    return new Color(1.0 - this._r, 1.0 - this._g, 1.0 - this._b, this._a)
  }

  get r() {
    return this._r
  }

  get g() {
    return this._g
  }

  get b() {
    return this._b
  }

  get a() {
    return this._a
  }

  toRGB() {
    return `rgb(${Math.round(255 * this._r)},${Math.round(255 * this._g)},${Math.round(255 * this._b)})`
  }

  toRGBA() {
    return `rgba(${Math.round(255 * this._r)},${Math.round(255 * this._g)},${Math.round(255 * this._b)},${this._a})`
  }

  toHex() {
    return (
      "#" +
      Math.round(this._r * 255)
        .toString(16)
        .padStart(2, "0") +
      Math.round(this._g * 255)
        .toString(16)
        .padStart(2, "0") +
      Math.round(this._b * 255)
        .toString(16)
        .padStart(2, "0")
    )
  }

  toHexA() {
    return (
      this.toHex() +
      Math.round(this._a * 255)
        .toString(16)
        .padStart(2, "0")
    )
  }
}
