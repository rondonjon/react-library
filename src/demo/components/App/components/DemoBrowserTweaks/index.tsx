import { BrowserTweaks } from "../../../../../components/BrowserTweaks"
import { useTheme } from "../../../../../hooks/useTheme"

export const DemoBrowserTweaks = () => {
  const { background } = useTheme().palette
  return <BrowserTweaks allowDragging={false} allowOverScrolling={false} themeColor={background} />
}
