import { ReactNode } from "react"

export type PanelProps = {
  children: ReactNode
  title?: string
}
