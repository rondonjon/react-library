import { Headline } from "../../../../../components/Headline"
import { useThemeStyleCss } from "../../../../../hooks/useThemeStyleCss"
import { PanelProps } from "./types/PanelProps"

export const Panel = ({ children, title }: PanelProps) => {
  const { spacing } = useThemeStyleCss()

  return (
    <div
      css={{
        display: "flex",
        flexDirection: "column",
        gap: spacing(0.5),
        padding: spacing(1),
      }}
    >
      {title && <Headline level={5}>{title}</Headline>}
      {children}
    </div>
  )
}
