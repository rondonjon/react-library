import { ReactNode } from "react"
import { useThemeStyleCss } from "../../../../../hooks/useThemeStyleCss"

export const Aside = ({ children }: { children: ReactNode }) => {
  const { spacing } = useThemeStyleCss()

  return (
    <aside
      css={{
        display: "flex",
        flexDirection: "column",
        gap: spacing(2),
      }}
    >
      {children}
    </aside>
  )
}
