import { Link } from "../../../../../components/Link"
import { Paragraph } from "../../../../../components/Paragraph"
import { useThemeStyleCss } from "../../../../../hooks/useThemeStyleCss"
import { demos } from "../../../../data/demos"
import { useViewport } from "../../hooks/useViewport"
import { Panel } from "../Panel"

export const Menu = () => {
  const { spacing } = useThemeStyleCss()
  const isCompact = useViewport() === "mobile"

  return (
    <Panel title="Menu">
      <div>
        {demos.map((demo) => (
          <div
            css={{
              display: isCompact ? "inline-block" : undefined,
              marginRight: spacing(0.5, 0.5),
            }}
            key={demo.anchor}
          >
            <Paragraph>
              <Link href={"#" + demo.anchor}>{demo.name}</Link>
            </Paragraph>
          </div>
        ))}
      </div>
    </Panel>
  )
}
