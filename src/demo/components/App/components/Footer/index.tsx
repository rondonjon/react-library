import { Link } from "../../../../../components/Link"
import { Paragraph } from "../../../../../components/Paragraph"
import pkg from "../../../../../../package.json"
import { FooterProps } from "./types/FooterProps"
import { Panel } from "../Panel"

export const Footer = ({ textAlign }: FooterProps) => (
  <footer>
    <Panel>
      <Paragraph textAlign={textAlign}>
        Copyright &copy;{" "}
        <Link href={pkg.author.url} target="_blank">
          {pkg.author.name}
        </Link>
        &nbsp;&bull;&nbsp; Version {pkg.version} &nbsp;&bull;&nbsp;
        <Link href={pkg.repository.url} target="_blank">
          README / License
        </Link>
      </Paragraph>
    </Panel>
  </footer>
)
