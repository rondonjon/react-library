import { HeadlineProps } from "../../../../../../components/Headline/types"

export type FooterProps = {
  textAlign?: HeadlineProps["textAlign"]
}
