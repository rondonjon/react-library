import { useState } from "react"
import { Splitter } from "../../../../../components/Splitter"
import { useTheme } from "../../../../../hooks/useTheme"
import { useThemeStyleCss } from "../../../../../hooks/useThemeStyleCss"
import { useViewport } from "../../hooks/useViewport"
import { Aside } from "../Aside"
import { Main } from "../Main"
import { ResponsiveLayoutProps } from "./types/ResponsiveLayoutProps"

export const ResponsiveLayout = ({ header, menu, settings, preview, footer }: ResponsiveLayoutProps) => {
  const splitPosition = useState(0.25)
  const viewport = useViewport()
  const theme = useTheme()
  const { spacing } = useThemeStyleCss()

  return (
    <div
      css={{
        alignContent: "start",
        background: theme.palette.background.toRGBA(),
        display: "flex",
        flexDirection: "column",
        gap: spacing(1),
        height: viewport === "desktop" ? "100vh" : undefined,
        minHeight: "100vh",
        maxHeight: viewport === "desktop" ? "100vh" : undefined,
        width: "100%",
      }}
    >
      {viewport === "desktop" ? (
        <>
          {header}
          <Splitter
            orientation="horizontal"
            a={
              <Aside>
                {menu}
                {settings}
              </Aside>
            }
            b={<Main>{preview}</Main>}
            position={splitPosition}
          />
          {footer}
        </>
      ) : (
        <>
          {header}
          {menu}
          {preview}
          {settings}
          {footer}
        </>
      )}
    </div>
  )
}
