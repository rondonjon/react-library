import { ReactNode } from "react"

export type ResponsiveLayoutProps = {
  header: ReactNode
  menu: ReactNode
  settings: ReactNode
  preview: ReactNode
  footer: ReactNode
}
