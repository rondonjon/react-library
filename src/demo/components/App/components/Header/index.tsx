import { Headline } from "../../../../../components/Headline"
import pkg from "../../../../../../package.json"
import { Panel } from "../Panel"
import { HeaderProps } from "./types/HeaderProps"

export const Header = ({ textAlign }: HeaderProps) => (
  <header>
    <Panel>
      <Headline level={2} textAlign={textAlign}>
        {pkg.description}
      </Headline>
    </Panel>
  </header>
)
