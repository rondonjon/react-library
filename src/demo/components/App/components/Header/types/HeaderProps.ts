import { ParagraphProps } from "../../../../../../components/Paragraph/types/ParagraphProps"

export type HeaderProps = {
  textAlign?: ParagraphProps["textAlign"]
}
