import { ReactNode } from "react"

export const Main = ({ children }: { children: ReactNode }) => (
  <main
    css={{
      display: "grid",
      minHeight: "100%",
    }}
  >
    {children}
  </main>
)
