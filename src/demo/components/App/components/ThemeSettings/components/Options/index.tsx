import { ReactNode } from "react"

export const Options = ({ children }: { children: ReactNode }) => (
  <div
    css={{
      display: "inline-flex",
      gap: "0.2rem",
      verticalAlign: "middle",
    }}
  >
    {children}
  </div>
)
