import { Children, ReactNode } from "react"

export const Setting = ({ children }: { children: ReactNode }) => (
  <div
    css={{
      display: "table-row",
    }}
  >
    {Children.map(children, (c) => (
      <div
        css={{
          display: "table-cell",
          padding: "0.4rem 0.2rem",
          verticalAlign: "middle",
        }}
      >
        {c}
      </div>
    ))}
  </div>
)
