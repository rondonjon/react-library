import { useCallback } from "react"
import { Button } from "../../../../../components/Button"
import { Monospace } from "../../../../../components/Monospace"
import { Paragraph } from "../../../../../components/Paragraph"
import { Theme } from "../../../../../types/Theme"
import { useSettings } from "../../hooks/useSettings"
import { Mode } from "../../types/Mode"
import { Panel } from "../Panel"
import { Options } from "./components/Options"
import { Setting } from "./components/Setting"

export const ThemeSettings = () => {
  const [state, setState] = useSettings()

  const updateStyle = useCallback(
    (style: Partial<Theme["style"]>) => {
      setState((prev) => ({
        ...prev,
        preview: {
          ...prev.preview,
          style: {
            ...prev.preview.style,
            ...style,
          },
        },
      }))
    },
    [setState]
  )

  const setMode = useCallback(
    (mode: Mode) => {
      setState((prev) => ({
        ...prev,
        preview: {
          ...prev.preview,
          mode,
        },
      }))
    },
    [setState]
  )

  return (
    <Panel title="Theme">
      <div>
        <Setting>
          <Paragraph>Mode</Paragraph>
          <Monospace>{state.preview.mode}</Monospace>
          <Options>
            <Button onClick={() => setMode("light")} label="light" />
            <Button onClick={() => setMode("dark")} label="dark" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Fill</Paragraph>
          <Monospace>{state.preview.style.fill.toFixed(3)}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ fill: 0.0 })} label="min" />
            <Button onClick={() => updateStyle({ fill: 0.5 })} label="med" />
            <Button onClick={() => updateStyle({ fill: 1.0 })} label="max" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Slope</Paragraph>
          <Monospace>{state.preview.style.slope.toFixed(3)}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ slope: 0.0 })} label="min" />
            <Button onClick={() => updateStyle({ slope: 0.5 })} label="med" />
            <Button onClick={() => updateStyle({ slope: 1.0 })} label="max" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Light</Paragraph>
          <Monospace>{state.preview.style.light.toFixed(3)}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ light: 0.0 })} label="min" />
            <Button onClick={() => updateStyle({ light: 0.5 })} label="med" />
            <Button onClick={() => updateStyle({ light: 1.0 })} label="max" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Roundness</Paragraph>
          <Monospace>{state.preview.style.roundness.toFixed(3)}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ roundness: 0.0 })} label="min" />
            <Button onClick={() => updateStyle({ roundness: 0.5 })} label="med" />
            <Button onClick={() => updateStyle({ roundness: 1.0 })} label="max" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Spacing</Paragraph>
          <Monospace>{state.preview.style.spacing.toFixed(3)}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ spacing: 0.0 })} label="min" />
            <Button onClick={() => updateStyle({ spacing: 0.5 })} label="med" />
            <Button onClick={() => updateStyle({ spacing: 1.0 })} label="max" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Text Scaling</Paragraph>
          <Monospace>{state.preview.style.textScaling.toFixed(3)}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ textScaling: 0.5 })} label="low" />
            <Button onClick={() => updateStyle({ textScaling: 1.0 })} label="normal" />
            <Button onClick={() => updateStyle({ textScaling: 2.0 })} label="high" />
          </Options>
        </Setting>
        <Setting>
          <Paragraph>Non-Text Units</Paragraph>
          <Monospace>{state.preview.style.nonTextUnits === "absolute" ? "abs" : "text-prop"}</Monospace>
          <Options>
            <Button onClick={() => updateStyle({ nonTextUnits: "absolute" })} label="abs" />
            <Button onClick={() => updateStyle({ nonTextUnits: "text-proportional" })} label="text-prop" />
          </Options>
        </Setting>
      </div>
    </Panel>
  )
}
