import { useMemo } from "react"
import { useSettings } from "../../hooks/useSettings"
import { Theme } from "../../../../../types/Theme"
import { useTheme } from "../../../../../hooks/useTheme"
import { Panel } from "../Panel"
import { defaultDarkTheme, defaultLightTheme } from "../../../../../data/themes"
import { useLocation } from "../../../../../hooks/useLocation"
import { demos } from "../../../../data/demos"
import { ThemeProvider } from "../../../../../providers/ThemeProvider"
import { InterceptingLocationProvider } from "../../../../../providers/InterceptingLocationProvider"
import { Paragraph } from "../../../../../components/Paragraph"
import { useThemeStyleCss } from "../../../../../hooks/useThemeStyleCss"

export const Preview = () => {
  const { current: location } = useLocation()
  const theme = useTheme()
  const [
    {
      preview: { style, mode },
    },
  ] = useSettings()
  const { roundness, spacing } = useThemeStyleCss()

  const previewTheme = useMemo<Theme>(
    () => ({
      ...(mode === "light" ? defaultLightTheme : defaultDarkTheme),
      style,
      fonts: theme.fonts,
    }),
    [mode, style, theme.fonts]
  )

  const Demo = useMemo(() => {
    const { hash } = new URL(location)
    return demos.find((demo) => hash === "#" + demo.anchor)?.component
  }, [location])

  return (
    <Panel title="Preview">
      <div
        css={{
          padding: "1rem",
        }}
      >
        {Demo ? (
          <ThemeProvider value={previewTheme}>
            <InterceptingLocationProvider>
              <div
                css={{
                  background: previewTheme.palette.background.toRGBA(),
                  borderRadius: roundness(1),
                  display: "grid",
                  gridTemplateColumns: "1fr",
                  gap: spacing(1.5),
                  padding: spacing(2),
                  width: "100%",
                  justifyItems: "start",
                  alignContent: "start",
                }}
              >
                <Demo />
              </div>
            </InterceptingLocationProvider>
          </ThemeProvider>
        ) : (
          <Paragraph>Please select a demo from the menu to enable this view.</Paragraph>
        )}
      </div>
    </Panel>
  )
}
