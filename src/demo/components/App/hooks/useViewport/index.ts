import { useContext } from "react"
import { viewportContext } from "../../contexts/viewport"

export const useViewport = () => useContext(viewportContext)
