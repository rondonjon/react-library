import { useContext } from "react"
import { settingsContext } from "../../contexts/settings"

export const useSettings = () => useContext(settingsContext)
