import { AppProps } from "./types/AppProps"
import { WindowLocationProvider } from "../../../providers/WindowLocationProvider"
import { ResponsiveLayout } from "./components/ResponsiveLayout"
import { Header } from "./components/Header"
import { Menu } from "./components/Menu"
import { ThemeSettings } from "./components/ThemeSettings"
import { Preview } from "./components/Preview"
import { SettingsProvider } from "../../providers/SettingsProvider"
import { ResponsiveViewportProvider } from "../../providers/ResponsiveViewportProvider"
import { Footer } from "./components/Footer"
import { userSelectContext } from "../../../contexts/userSelect"
import { DemoBrowserTweaks } from "./components/DemoBrowserTweaks"
import { SelectedThemeProvider } from "../../providers/SelectedThemeProvider"

export const App = ({ fonts }: AppProps) => (
  <userSelectContext.Provider value={false}>
    <WindowLocationProvider>
      <ResponsiveViewportProvider>
        <SettingsProvider>
          <SelectedThemeProvider fonts={fonts}>
            <DemoBrowserTweaks />
            <ResponsiveLayout
              header={<Header textAlign="center" />}
              menu={<Menu />}
              settings={<ThemeSettings />}
              preview={<Preview />}
              footer={<Footer textAlign="center" />}
            />
          </SelectedThemeProvider>
        </SettingsProvider>
      </ResponsiveViewportProvider>
    </WindowLocationProvider>
  </userSelectContext.Provider>
)
