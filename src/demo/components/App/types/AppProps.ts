import { Theme } from "../../../../types/Theme"

export type AppProps = {
  fonts: Theme["fonts"]
}
