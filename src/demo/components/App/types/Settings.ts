import { Theme } from "../../../../types/Theme"
import { Mode } from "./Mode"

export type Settings = {
  preview: {
    mode: Mode
    style: Theme["style"]
  }
}
