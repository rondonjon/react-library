import { createContext } from "react"
import { ViewportContextProps } from "./types/ViewportContextProps"

export const viewportContext = createContext<ViewportContextProps>("mobile")
