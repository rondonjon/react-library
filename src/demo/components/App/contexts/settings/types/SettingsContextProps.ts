import { Dispatch, SetStateAction } from "react"
import { Settings } from "../../../types/Settings"

type SetState<T> = [T, Dispatch<SetStateAction<T>>]

export type SettingsContextProps = SetState<Settings>
