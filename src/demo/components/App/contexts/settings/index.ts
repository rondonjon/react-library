import { createContext } from "react"
import { defaultDarkTheme } from "../../../../../data/themes"
import { SettingsContextProps } from "./types/SettingsContextProps"

export const settingsContext = createContext<SettingsContextProps>([
  {
    preview: {
      mode: "dark",
      style: defaultDarkTheme.style,
    },
  },
  () => {
    throw new Error("not implemented")
  },
])
