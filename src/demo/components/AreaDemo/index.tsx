import { Area } from "../../../components/Area"
import { Paragraph } from "../../../components/Paragraph"

export const AreaDemo = () => (
  <>
    {(["s", "m", "l"] as const).map((padding) => (
      <div key={padding}>
        <Paragraph size="l">Padding = {padding}</Paragraph>
        <Area padding={padding}>
          <Area padding={padding}>
            <Area padding={padding}>
              <Paragraph>Area Demo</Paragraph>
            </Area>
          </Area>
        </Area>
      </div>
    ))}
  </>
)
