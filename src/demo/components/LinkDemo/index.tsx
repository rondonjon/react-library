import { Link } from "../../../components/Link"
import { Paragraph } from "../../../components/Paragraph"

export const LinkDemo = () => (
  <Paragraph>
    Lorem ipsum dolor <Link href="/linkdemo/">link</Link> amet.
  </Paragraph>
)
