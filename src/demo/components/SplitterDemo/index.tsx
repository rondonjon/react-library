import { useState } from "react"
import { Splitter } from "../../../components/Splitter"
import { SizeBox } from "./components/SizeBox"

export const SplitterDemo = () => {
  const posSplitterV1 = useState<number>(0.5)
  const posSplitterV2 = useState<number>(0.5)
  const posSplitterH = useState<number>(0.5)

  return (
    <Splitter
      height="16rem"
      orientation="horizontal"
      position={posSplitterH}
      width="100%"
      a={<Splitter orientation="vertical" a={<SizeBox />} b={<SizeBox />} position={posSplitterV1} />}
      b={<Splitter orientation="vertical" a={<SizeBox />} b={<SizeBox />} position={posSplitterV2} />}
    />
  )
}
