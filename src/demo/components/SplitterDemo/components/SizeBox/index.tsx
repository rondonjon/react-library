import { Paragraph } from "../../../../../components/Paragraph"
import { useClientRect } from "../../../../../hooks/useClientRect"

export const SizeBox = () => {
  const { ref, width, height } = useClientRect()

  return (
    <div
      css={{
        alignContent: "center",
        display: "grid",
        height: "100%",
        justifyContent: "center",
        width: "100%",
      }}
      ref={ref}
    >
      <Paragraph>
        {Math.round(width || 0)} x {Math.round(height || 0)}
      </Paragraph>
    </div>
  )
}
