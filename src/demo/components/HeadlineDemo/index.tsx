import { Headline } from "../../../components/Headline"

export const HeadlineDemo = () => (
  <>
    <Headline level={1}>Lorem ipsum dolor sit amet</Headline>
    <Headline level={2}>Lorem ipsum dolor sit amet</Headline>
    <Headline level={3}>Lorem ipsum dolor sit amet</Headline>
    <Headline level={4}>Lorem ipsum dolor sit amet</Headline>
    <Headline level={5}>Lorem ipsum dolor sit amet</Headline>
    <Headline level={6}>Lorem ipsum dolor sit amet</Headline>
  </>
)
