import { Paragraph } from "../../../components/Paragraph"

export const ParagraphDemo = () => (
  <>
    <Paragraph size="l">Lorem ipsum dolor sit amet.</Paragraph>
    <Paragraph size="m">Lorem ipsum dolor sit amet.</Paragraph>
    <Paragraph size="s">Lorem ipsum dolor sit amet.</Paragraph>
  </>
)
