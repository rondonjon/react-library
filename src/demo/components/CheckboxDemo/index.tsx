import { useState } from "react"
import { Checkbox } from "../../../components/Checkbox"
import { Label } from "../../../components/Label"
import { Row } from "./components/Row"

export const CheckboxDemo = () => {
  const [value, setValue] = useState(true)

  return (
    <>
      <Row>
        <Checkbox id="checkbox-demo-1" name="checkbox-demo-1" value={value} onChange={setValue} />
        <Label for="checkbox-demo-1">Accept</Label>
      </Row>
      <Row>
        <Checkbox id="checkbox-demo-2" name="checkbox-demo-2" value={false} onChange={setValue} isDisabled />
        <Label for="checkbox-demo-2">Disabled</Label>
      </Row>
    </>
  )
}
