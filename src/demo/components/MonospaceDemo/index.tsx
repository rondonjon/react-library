import { Monospace } from "../../../components/Monospace"
import { Paragraph } from "../../../components/Paragraph"

export const MonospaceDemo = () => (
  <>
    <Paragraph>
      Lorem ipsum dolor <Monospace type="inline">inline</Monospace> amet.
    </Paragraph>
    <Monospace type="inline">{`function hi() {\n  window.alert("hi");\n}`}</Monospace>
    <Monospace type="preformatted-block">{`function hi() {\n  window.alert("hi");\n}`}</Monospace>
  </>
)
