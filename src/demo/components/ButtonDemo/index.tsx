import { Button } from "../../../components/Button"

export const ButtonDemo = () => (
  <>
    <Button label="click me" href="#" type="primary" />
    <Button label="click me" href="#" />
    <Button label="click me" href="#" type="danger" />
    <Button label="unclickable" href="#" isDisabled />
  </>
)
