import { useCallback, useState } from "react"
import { Label } from "../../../components/Label"
import { Paragraph } from "../../../components/Paragraph"
import { RadioButton } from "../../../components/RadioButton"
import { Row } from "./components/Row"

export const RadioButtonDemo = () => {
  const [value, setValue] = useState("A")

  const handleChange = useCallback((isChecked: boolean, value: string) => {
    if (isChecked) {
      setValue(value)
    }
  }, [])

  return (
    <>
      <Row>
        <Paragraph>Your current selection is: {value}</Paragraph>
      </Row>
      <Row>
        <RadioButton id="optionA" name="radio-button-demo" value="A" onChange={handleChange} isChecked={value === "A"} />
        <Label for="optionA">Option A</Label>
      </Row>
      <Row>
        <RadioButton id="optionB" name="radio-button-demo" value="B" onChange={handleChange} isChecked={value === "B"} />
        <Label for="optionB">Option B</Label>
      </Row>
      <Row>
        <RadioButton id="optionC" name="radio-button-demo" value="C" onChange={handleChange} isChecked={value === "C"} />
        <Label for="optionC">Option C</Label>
      </Row>
      <Row>
        <RadioButton id="optionD" name="radio-button-demo" value="D" onChange={handleChange} isDisabled isChecked={value === "D"} />
        <Label for="optionD">Option D (disabled)</Label>
      </Row>
    </>
  )
}
