import { ReactNode } from "react"
import { useThemeStyleCss } from "../../../../../hooks/useThemeStyleCss"

export const Row = ({ children }: { children: ReactNode }) => {
  const { spacing } = useThemeStyleCss()
  return (
    <div
      css={{
        alignItems: "center",
        display: "flex",
        gap: spacing(1),
      }}
    >
      {children}
    </div>
  )
}
