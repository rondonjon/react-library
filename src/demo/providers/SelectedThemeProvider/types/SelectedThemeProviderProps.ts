import { ReactNode } from "react"
import { Theme } from "../../../../types/Theme"

export type SelectedThemeProviderProps = {
  children: ReactNode
  fonts: Theme["fonts"]
}
