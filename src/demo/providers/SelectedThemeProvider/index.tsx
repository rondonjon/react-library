import { useMemo } from "react"
import { defaultDarkTheme, defaultLightTheme } from "../../../data/themes"
import { ThemeProvider } from "../../../providers/ThemeProvider"
import { Theme } from "../../../types/Theme"
import { useSettings } from "../../components/App/hooks/useSettings"
import { SelectedThemeProviderProps } from "./types/SelectedThemeProviderProps"

export const SelectedThemeProvider = ({ children, fonts }: SelectedThemeProviderProps) => {
  const [
    {
      preview: { mode },
    },
  ] = useSettings()

  const theme = useMemo<Theme>(
    () => ({
      ...(mode === "light" ? defaultDarkTheme : defaultLightTheme),
      fonts,
    }),
    [mode, fonts]
  )

  return <ThemeProvider value={theme}>{children}</ThemeProvider>
}
