import { useClientRect } from "../../../hooks/useClientRect"
import { viewportContext } from "../../components/App/contexts/viewport"
import { ResponsiveViewportProviderProps } from "./types/ResponsiveViewportProviderProps"

export const ResponsiveViewportProvider = ({ children }: ResponsiveViewportProviderProps) => {
  const { ref, width = 0 } = useClientRect()

  return (
    <div ref={ref}>
      <viewportContext.Provider value={width < 400 ? "mobile" : width < 960 ? "tablet" : "desktop"}>{children}</viewportContext.Provider>
    </div>
  )
}
