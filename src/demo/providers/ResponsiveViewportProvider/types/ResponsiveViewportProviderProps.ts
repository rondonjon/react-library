import { ReactNode } from "react"

export type ResponsiveViewportProviderProps = {
  children: ReactNode
}
