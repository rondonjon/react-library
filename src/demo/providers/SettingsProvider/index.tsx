import { useState } from "react"
import { defaultDarkTheme } from "../../../data/themes"
import { settingsContext } from "../../components/App/contexts/settings"
import { Settings } from "../../components/App/types/Settings"
import { SettingsProviderProps } from "./types/SettingsProviderProps"

export const SettingsProvider = ({ children }: SettingsProviderProps) => {
  const value = useState<Settings>({
    preview: {
      mode: "dark",
      style: defaultDarkTheme.style,
    },
  })

  return <settingsContext.Provider value={value}>{children}</settingsContext.Provider>
}
