import { ReactNode } from "react"

export type SettingsProviderProps = {
  children: ReactNode
}
