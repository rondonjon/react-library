import { AreaDemo } from "../components/AreaDemo"
import { ButtonDemo } from "../components/ButtonDemo"
import { CheckboxDemo } from "../components/CheckboxDemo"
import { HeadlineDemo } from "../components/HeadlineDemo"
import { LinkDemo } from "../components/LinkDemo"
import { MonospaceDemo } from "../components/MonospaceDemo"
import { ParagraphDemo } from "../components/ParagraphDemo"
import { RadioButtonDemo } from "../components/RadioButtonDemo"
import { SplitterDemo } from "../components/SplitterDemo"

export const demos = [
  {
    anchor: "area",
    name: "Area",
    component: AreaDemo,
  },
  {
    anchor: "button",
    name: "Button",
    component: ButtonDemo,
  },
  {
    anchor: "checkbox",
    name: "Checkbox",
    component: CheckboxDemo,
  },
  {
    anchor: "headline",
    name: "Headline",
    component: HeadlineDemo,
  },
  {
    anchor: "link",
    name: "Link",
    component: LinkDemo,
  },
  {
    anchor: "monospace",
    name: "Monospace",
    component: MonospaceDemo,
  },
  {
    anchor: "paragraph",
    name: "Paragraph",
    component: ParagraphDemo,
  },
  {
    anchor: "radiobutton",
    name: "Radio Button",
    component: RadioButtonDemo,
  },
  {
    anchor: "splitter",
    name: "Splitter",
    component: SplitterDemo,
  },
]
