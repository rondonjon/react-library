import ReactDOM from "react-dom/client"
import { App } from "./components/App"
import { Theme } from "../types/Theme"
import "destyle.css"
import "@fontsource/quicksand/latin-400.css"
import "@fontsource/inconsolata/400.css"
import "@fontsource/assistant/400.css"

const root = document.getElementById("root")

const fonts: Theme["fonts"] = {
  headline: {
    family: "Quicksand",
    weight: 400,
  },
  monospace: {
    family: "Inconsolata",
    weight: 400,
  },
  paragraph: {
    family: "Assistant",
    weight: 400,
  },
}

if (root) {
  ReactDOM.createRoot(root).render(<App fonts={fonts} />)
}
