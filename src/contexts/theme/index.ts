import { createContext } from "react"
import { defaultLightTheme } from "../../data/themes"
import { ThemeContextProps } from "./types/ThemeContextProps"

export const themeContext = createContext<ThemeContextProps>(defaultLightTheme)
