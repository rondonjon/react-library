import { Theme } from "../../../types/Theme"

export type ThemeContextProps = Theme
