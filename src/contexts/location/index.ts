import { createContext } from "react"
import { LocationContextProps } from "./types/LocationContextProps"

export const defaultLocation = {
  current: "",
  set() {
    throw new Error("not implemented")
  },
}

export const locationContext = createContext<LocationContextProps>(defaultLocation)
