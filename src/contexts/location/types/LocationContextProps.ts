export type LocationContextProps = {
  current: Readonly<string>
  set: (location: string) => void
}
