import { createContext } from "react"

export const userSelectContext = createContext<boolean>(true)
